#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#===============================================================================
# Written by D.Burnand from Rentouch GmbH 2013 - http://www.rentouch.ch
#===============================================================================

import os
import subprocess

def install():
    name= 'berkelium-1.3.linux-x86_64.kex'
    version= 'berkelium_1.3'
    
    #download berkelium
    os.system('wget http://jegger.ch/datapool/probazaar/'+name+' -q')
    
    #remove old berkeliums
    os.system('rm -r -f ~/.kivy/extensions/_consumed_zips/'+name)
    os.system('rm -r -f ~/.kivy/extensions/'+name)
    os.system('rm -r -f ~/.kivy/extensions/'+version)
    
    #copy file to extensions
    os.system('cp '+name+' ~/.kivy/extensions/'+name)
    
    #remove file
    os.system('rm -r -f '+name)


install()